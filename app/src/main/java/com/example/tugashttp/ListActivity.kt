package com.example.tugashttp

import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.ListView
import androidx.appcompat.app.AppCompatActivity
import android.R.layout.simple_list_item_1
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import org.json.JSONArray
import org.json.JSONObject


class ListActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list)
        val listView: ListView = findViewById(R.id.list_view)

        val result = JSONArray(httpRequest("https://jsonplaceholder.typicode.com/users").body()?.string())
        listView.adapter = ArrayAdapter(this, simple_list_item_1, parseName(result))
    }

    fun httpRequest(url: String): Response {
        return OkHttpClient().newCall(
            Request
                .Builder()
                .get()
                .url(url)
                .build()
        )
            .execute()
    }

    fun parseName(jsonArray: JSONArray): ArrayList<String> {
        val list = arrayListOf<String>()
        for (i in 0 until jsonArray.length() - 1) {
            val jsonObject = JSONObject(jsonArray.opt(i).toString())
            list.add(jsonObject.getString("name"))
        }
        return list
    }
}