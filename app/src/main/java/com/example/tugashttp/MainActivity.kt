package com.example.tugashttp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import okhttp3.OkHttpClient
import okhttp3.Request
import android.os.StrictMode
import android.os.StrictMode.ThreadPolicy
import android.widget.EditText
import android.widget.Toast
import com.google.android.material.textfield.TextInputEditText
import okhttp3.Response
import org.json.JSONArray
import org.json.JSONObject

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        StrictMode.setThreadPolicy(ThreadPolicy.Builder().permitAll().build())

        findViewById<Button>(R.id.loginBtn).setOnClickListener {
            doLogin()
        }
    }

    fun doLogin () {
        val username = findViewById<EditText>(R.id.usernameInput).text.toString()
        val email = findViewById<EditText>(R.id.emailInput).text.toString()
        val result = JSONArray(httpRequest("https://jsonplaceholder.typicode.com/users").body()?.string())
        val isCorrect = checkAccount(result, username, email)

        if(isCorrect) {
            startActivity(Intent(this, ListActivity::class.java))
        } else {
            Toast.makeText(this, isCorrect.toString(), Toast.LENGTH_SHORT).show()
        }
    }

    fun httpRequest(url: String): Response {
       return OkHttpClient().newCall(
            Request
                .Builder()
                .get()
                .url(url)
                .build()
        )
            .execute()
    }

    fun checkAccount(jsonArray: JSONArray, username: String, email: String): Boolean {
        for (i in 0 until jsonArray.length() - 1) {
            val jsonObject = JSONObject(jsonArray.opt(i).toString())
            if (jsonObject.getInt("id") == 1) {
                val usernameResponse = jsonObject.getString("username")
                val emailResponse = jsonObject.getString("email")
                return usernameResponse == username && emailResponse == email
            }
        }
        return false
    }
}